SET IDENTITY_INSERT [dbo].[CONCEPTOS] ON 
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (1, 1, -1, N'Estados', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (2, 1, 0, N'Activo', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (3, 1, 1, N'Eliminado', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (4, 1, 2, N'Deshabilitado', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (5, 2, -1, N'Tiene Covid', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (6, 2, 0, N'No Tiene', 1)
GO
INSERT [dbo].[CONCEPTOS] ([con_id], [con_prefijo], [con_correlativo], [con_descripcion], [con_estado]) VALUES (7, 2, 1, N'Tiene', 1)
GO
SET IDENTITY_INSERT [dbo].[CONCEPTOS] OFF
GO
SET IDENTITY_INSERT [dbo].[Mensaje] ON 
GO
INSERT [dbo].[Mensaje] ([Id], [cabecera], [contenido]) VALUES (1, N'Recordatorio: Auto Reporte COVID-19', N'No olvides llenar tu auto reporte de hoy. IDE Solution te cuida.')
GO
SET IDENTITY_INSERT [dbo].[Mensaje] OFF
GO
SET IDENTITY_INSERT [dbo].[Perfil] ON 
GO
INSERT [dbo].[Perfil] ([Per_Id], [Per_Detalle]) VALUES (1, N'Practicante')
GO
INSERT [dbo].[Perfil] ([Per_Id], [Per_Detalle]) VALUES (2, N'Administrador')
GO
INSERT [dbo].[Perfil] ([Per_Id], [Per_Detalle]) VALUES (3, N'Jefe de �rea')
GO
INSERT [dbo].[Perfil] ([Per_Id], [Per_Detalle]) VALUES (4, N'Analista')
GO
INSERT [dbo].[Perfil] ([Per_Id], [Per_Detalle]) VALUES (5, N'Varios')
GO
SET IDENTITY_INSERT [dbo].[Perfil] OFF
GO
SET IDENTITY_INSERT [dbo].[SIGNOS] ON 
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (1, N'Disnea (Dificultad en respirar)')
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (2, N'Taquipnea ( Menor o igual 22 rpm)')
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (3, N'Saturaci�n de ox�geno (menor a 92%)')
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (4, N'Alteraci�n de la conciencia')
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (5, N'N/A')
GO
INSERT [dbo].[SIGNOS] ([Sig_Id], [Sig_Detalle]) VALUES (6, N'Otros')
GO
SET IDENTITY_INSERT [dbo].[SIGNOS] OFF
GO
SET IDENTITY_INSERT [dbo].[SINTOMAS] ON 
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (1, N'Fiebre')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (2, N'Escalofr�os')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (3, N'Tos')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (4, N'Dificultad Respiratoria')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (5, N'Dolor de Garganta')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (6, N'Congesti�n Nasal')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (7, N'Perdida del sentido del olfato y/o el gusto')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (8, N'Dolor de cabeza')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (9, N'Nauseas o v�mitos')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (10, N'Dolor muscular')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (11, N'Dolor de pecho')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (12, N'Irritabilidad / confusi�n')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (13, N'N/A')
GO
INSERT [dbo].[SINTOMAS] ([Sint_Id], [Sint_Detalle]) VALUES (14, N'Otros')
GO
SET IDENTITY_INSERT [dbo].[SINTOMAS] OFF
GO
SET IDENTITY_INSERT [dbo].[UsuarioIDE] ON 
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (36, N'43877763', N'Juan Carlos', N'Alarc�n Saavedra', N'966527494', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jalarcon@ide-solution.com', N'Pasaje La Puerta 447-A Urb. Remigio Silva ', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (37, N'47396816', N'Juan Armando', N'Alfaro Pary Fleming', N'942833546', N'0x81DC9BDB52D04DC20036DBD8313ED055', 3, 0, N'falfaro@ide-solution.com', N'Urbanizaci�n San Miguel Mz. A Lt. 4', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (38, N'43877778', N'Christian Jesus', N'Ayasta Vidaurre ', N'972182676', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'cayasta@ide-solution.com', N'Calle Danubio 109 B, P.J. San Nicol�s', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (39, N'47370702', N' Pierre Irving', N'Barrera Pareja', N'990227981', N'0x81DC9BDB52D04DC20036DBD8313ED055', 2, 0, N'pbarrera@ide-solution.com�', N'Urb. Los Jardines de Santa Rosa Mz G Lt 1', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (40, N'73668208', N'Miccy Lissethe', N'Bonilla Banda ', N'999661395', N'0x81DC9BDB52D04DC20036DBD8313ED055', 2, 0, N'mbonilla@ide-solution.com�', N'Urb. Jardines de la Pradera Mz. BLT. 7', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (41, N'47722705', N'Rosalina', N'Capristan Piscoya ', N'931264062', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'rcapristan@ide-solution.com', N'CALLE EL AYLLU 197', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (42, N'42948152', N' Jos� Herm�genes', N'Chumioque Pisfil', N'929559907', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jchumioque@ide-solution.com', N'Villa Custodio S/N - Caser�o de Callanca', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (43, N'45681531', N' Cristhian Alexander', N'Colorado Soberon', N'978844685', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'ccolorado@ide-solution.com', N'Calle Germ�n Legu�a Mz.L Lt.11 Urb. Castilla de Oro', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (44, N'40425001', N'Karina Yulisa', N'Custodio Ayasta', N'949950474', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'kcustodio@ide-solution.com', N'Av. Pedro Ruiz 250', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (45, N'76430547', N'Jefferson Luis', N'Gal�n Zapata', N'966841797', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jgalan@ide-solution.com', N'Calle Teresa Fanning 318 Pueblo J. Jose Olaya ', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (46, N'74777326', N'Alex Rodulfo', N'Gonzales Siesqu�n', N'922831933', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'agonzales@ide-solution.com', N'Augusto B. Legu�a Mz. 2 Lt. 13 P.J. Sim�n Bol�var', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (47, N'73245222', N'Joseph Lenin', N'Guivar Rimapa', N'952446320', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jguivar@ide-solution.com', N'Calle San Cristobal 316 Pueblo Joven San Antonio', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (48, N'76577148', N' Carlos Alberto', N'Hern�ndez Hern�ndez', N'982604853', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'chernandez@ide-solution.com', N'Calle Trujillo Mz. H Lt. 9 P.J. Miraflores', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (49, N'46889371', N'Jonathan Joel', N'Macalupu Salazar', N'932152174', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jmacalupu@ide-solution.com', N'AV. Buenos Aires SN, Tum�n, Lambayeque,', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (50, N'47806986', N'Mar�a Magdalena', N'Mart�nez Cumpa', N'925938900', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'mmartinez@ide-solution.com', N'Calle Bolognesi 312', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (51, N'76010148', N' Carmen Rosa', N'Mart�nez Rodr�guez', N'96890999', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'cmartinez@ide-solution.com', N'Emiliano Ni�o N�354 - 09 de octubre', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (52, N'42604085', N'Julio Alex', N'Mendoza S�nchez', N'946223645', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jmendoza@ide-solution.com', N'MZ. B LT. 21 URB. Villas de la Ensenada', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (53, N'76367798', N'Cristian', N'Merino Ortiz', N'936913464', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'cmerino@ide-solution.com', N'Calle Los Nogales 202 P.J. 9 de octubre', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (54, N'72228803', N' Lady Dyan', N'Mio Villacorta', N'948833372', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'lmio@ide-solution.com', N'Calle Los Relatos 153 P.J. 9 de octubre', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (55, N'70224042', N'Jose Carlos', N'Niquen Alvarado', N'930457549', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'jniquen@ide-solution.com', N'Av. Prolongaci�n Venezuela 404 Lado Sur', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (56, N'44390435', N'Mar�a del Rosario', N'Olivos Ramirez', N'978752122', N'0x81DC9BDB52D04DC20036DBD8313ED055', 3, 0, N'rolivos@ide-solution.com', N'Urbanizaci�n San Miguel Mz. A Lt. 4', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (57, N'44413378', N'Ruben Michael', N'Pachamango Chunque', N'979574999', N'0x81DC9BDB52D04DC20036DBD8313ED055', 2, 0, N'rpachamango@ide-solution.com', N'Calle Bacamattos 729', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (58, N'72803974', N' Giset Iris ', N'Purizaga Araujo', N'993383822', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'gpurizaga@ide-solution.com', N'Av. Haya de La Torre 880 � Dpto. 101 � Urb. Las Moreras', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (59, N'46660168', N' Katherine Vanessa ', N'Quintos Estrada', N'979494145', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'kquintos@ide-solution.com', N'Prolongacion Huascar 128', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (60, N'45118318', N'Gonzalo Martin', N'Romero Abanto', N'951676464', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'gromero@ide-solution.com', N'Calle Nazca 208', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (61, N'74606636', N' Axel Jesus', N'Salda�a Lara', N'972798164', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'asaldana@ide-solution.com', N'Mz. A Lote 24 Urb. Santa Lila', N'�MÙ7�����} | !', 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (62, N'73941766', N'Angie Estefanie', N'Taboada Arellano', N'948264180', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'ataboada@ide-solution.com', N'av Eufemio lora y lora 1535', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (63, N'47941221', N'Alvaro Humberto', N'Torres Mimbela', N'994011407', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'amimbela@ide-solution.com', N'Calle Rio Coata #135 PJ. Luis Albers', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (64, N'70268784', N' Cristian Abelardo', N'Vallejos Fonseca', N'990939841', N'0x81DC9BDB52D04DC20036DBD8313ED055', 4, 0, N'cvallejos@ide-solution.com', N'Mz M Lt 22 Urb. Fermin Avila Moron', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (65, N'45930364', N'John Anthony', N'Villanueva Garc�a', N'950673642', N'0x81DC9BDB52D04DC20036DBD8313ED055', 3, 0, N'jvillanueva@ide-solution.com', N'Mz. N Lt. 2 A.H. San Ger�nimo Urb. La Pradera', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (66, N'75115452', N'Diego', N'Cruz Neciosup', N'945098050', N'0x81DC9BDB52D04DC20036DBD8313ED055', 1, 0, N'dcruz@ide-solution.com', N'calle ricardo palma 208- Pimentel', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (67, N'75739934', N' Francisco', N'Sosa Ru�z', N'927 022 672', N'0x81DC9BDB52D04DC20036DBD8313ED055', 1, 0, N'fsosa@ide-solution.com', N'Britaldo Gonzales 505 - Pueblo Nuevo - Ferre�afe', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (68, N'78551646', N'Felix Gregorio', N'Benavides Goicochea', N'984010045', N'0x81DC9BDB52D04DC20036DBD8313ED055', 1, 0, N'fbenavides@ide-solution.com', N'av. mexico #291 como referencia la Tupac Amaru ', NULL, 0, NULL)
GO
INSERT [dbo].[UsuarioIDE] ([User_Id], [User_Dni], [User_Nombres], [User_Apellidos], [User_Telefono], [User_Contrasenia], [User_Per_Id], [User_HasCovid], [User_Correo], [User_Direccion], [User_clave_temp], [User_Estado], [AppId]) VALUES (69, N'75904828', N'Sofia Paola', N'Manayalle Salda�a', N'977749504', N'0x81DC9BDB52D04DC20036DBD8313ED055', 1, 0, N'smanayalle@ide-solution.com ', N'Los Carrizos 150 - Enrique L�pez Albujar ', NULL, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[UsuarioIDE] OFF
GO
INSERT [dbo].[version] ([version]) VALUES (N'1.0.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'2.1.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'3.0.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'3.0.1')
GO
INSERT [dbo].[version] ([version]) VALUES (N'3.1.1')
GO
INSERT [dbo].[version] ([version]) VALUES (N'4.0.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'5.0.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'5.1.0')
GO
INSERT [dbo].[version] ([version]) VALUES (N'5.2.0')
GO
SET ANSI_PADDING ON
GO
