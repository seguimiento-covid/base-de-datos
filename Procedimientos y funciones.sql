/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSignosCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_RetornarSignosCovid] (@idRep INT)
RETURNS VARCHAR(800)
AS
BEGIN
	DECLARE @response VARCHAR(800);
	SELECT @response = COALESCE(@response + '|', '') +
	CASE 
		WHEN trans.TRANSC_SIG_ID = 6 THEN trans.TRANSC_OTRODETALLE
		ELSE sint.Sig_Detalle
	END
	FROM REPC_SIG_TRANS AS trans 
	INNER JOIN SIGNOS AS sint ON trans.TRANSC_SIG_ID = sint.Sig_Id
	WHERE trans.TRANSC_REPC_ID = @idRep
	RETURN @response;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSintomasCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_RetornarSintomasCovid] (@idRep INT)
RETURNS VARCHAR(800)
AS
BEGIN
	DECLARE @response VARCHAR(800);
	SELECT @response = COALESCE(@response + '|', '') +
	CASE 
		WHEN trans.TRANSSIN_SIN_ID = 14 THEN trans.TRANSSIN_OTRODETALLE
		ELSE sint.Sint_Detalle
	END
	FROM REPC_SINT_TRANS AS trans 
	INNER JOIN SINTOMAS AS sint ON trans.TRANSSIN_SIN_ID = sint.Sint_Id
	WHERE trans.TRANSSIN_REPC_ID = @idRep
	RETURN @response;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSintomasNormal]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_RetornarSintomasNormal] (@idRep INT)
RETURNS VARCHAR(800)
AS
BEGIN
	DECLARE @response VARCHAR(800);
	SELECT @response = COALESCE(@response + '|', '') +
	CASE 
		WHEN trans.TRANSC_Sint_id = 14 THEN trans.TRANSC_Otro
		ELSE sint.Sint_Detalle
	END
	FROM REP_SINT_TRANSC AS trans 
	INNER JOIN SINTOMAS AS sint ON trans.TRANSC_Sint_id = sint.Sint_Id
	WHERE trans.TRANSC_Rep_id = @idRep
	RETURN @response;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ValidarFechaAndUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_ValidarFechaAndUser](@fecha DATETIME,@userId int)
RETURNS bit
AS
BEGIN
	DECLARE @respuesta bit;
	IF EXISTS(SELECT TOP 1 * FROM REPORTE WHERE Rep_User_Id = @userId
	AND YEAR(@fecha) = YEAR(Rep_FechaRealizado) AND MONTH(@fecha) = MONTH(Rep_FechaRealizado) AND DAY(@fecha) = DAY(Rep_FechaRealizado)
	 ORDER BY Rep_FechaRealizado DESC)
	BEGIN
		SET @respuesta = CAST(0 AS bit);
	END
	ELSE
	BEGIN
		SET @respuesta = CAST(1 AS bit);
	END;
	RETURN @respuesta;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fnCustomPass]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnCustomPass] 
(    
    @size AS INT, --Tama�o de la cadena aleatoria
    @op AS VARCHAR(2) --Opci�n para letras(ABC..), numeros(123...) o ambos.
    -- C = LETRAS, N= NUMEROS, CN = AMBOS
)
RETURNS VARCHAR(62)
AS
BEGIN    

    DECLARE @chars AS VARCHAR(52),
            @numbers AS VARCHAR(10),
            @strChars AS VARCHAR(62),        
            @strPass AS VARCHAR(62),
            @index AS INT,
            @cont AS INT

    SET @strPass = ''
    SET @strChars = ''    
    SET @chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@-*+/.'
    SET @numbers = '0123456789'

    SET @strChars = CASE @op WHEN 'C' THEN @chars --Letras
                        WHEN 'N' THEN @numbers --N�meros
                        WHEN 'CN' THEN @chars + @numbers --Ambos (Letras y N�meros)
                        ELSE '------'
                    END

    SET @cont = 0
    WHILE @cont < @size
    BEGIN
        SET @index = ceiling( ( SELECT rnd FROM vwRandom ) * (len(@strChars)))--Uso de la vista para el Rand() y no generar error.
        SET @strPass = @strPass + substring(@strChars, @index, 1)
        SET @cont = @cont + 1
    END    
        
    RETURN @strPass

END
GO
/****** Object:  UserDefinedFunction [dbo].[responseDescripcionDia]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[responseDescripcionDia] 
(
@respuesta SMALLINT,
@idRep INT
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @resp VARCHAR(20);
	IF @respuesta = 2
		BEGIN
			SET @resp = 'CASO CONFIRMADO';
		END
	ELSE IF @respuesta = 1
		BEGIN
			SET @resp = 'CASO SOSPECHOSO';
		END
	ELSE IF @respuesta = 3
		BEGIN
			DECLARE @cantSint SMALLINT = (SELECT COUNT(*) FROM REP_SINT_TRANSC WHERE TRANSC_Sint_id <> 13 AND TRANSC_Rep_id = @idRep);
			IF @cantSint = 1
			BEGIN
				SET @resp = 'TIENE UN S�NTOMA';
			END
			ELSE
			BEGIN
				SET @resp = 'SIN S�NTOMAS';
			END
		END;
	ELSE 
		BEGIN
			SET @resp = '';
		END;
	RETURN @resp; 
END;
GO
/****** Object:  UserDefinedFunction [dbo].[retornarDias]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[retornarDias] (@idUser INT,@mes INT, @anio INT)
RETURNS VARCHAR(200)
AS 
BEGIN
	DECLARE @count INT =1; 
    DECLARE @finalDay Int  = (SELECT DAY(EOMONTH(GETDATE())));
    DECLARE @days VARCHAR(120) = ''; 
    WHILE(@count <= @finalDay)
    BEGIN
    IF EXISTS(SELECT * FROM REPORTE WHERE DAY(Rep_FechaRealizado) = @count AND Rep_User_Id = @idUser AND MONTH(Rep_FechaRealizado) = @mes AND YEAR(Rep_FechaRealizado) =@anio) 
    BEGIN
        SET @days = CONCAT(@days,@count,'/');   
    END
    ELSE
    BEGIN
        IF EXISTS(SELECT * FROM REPORTECOVID WHERE DAY(REPC_Fecha) = @count AND REPC_User_Id = @idUser AND MONTH(REPC_Fecha) = @mes AND YEAR(REPC_Fecha) =@anio )
        BEGIN
            SET @days = CONCAT(@days,@count,'/');   
        END
    END
            SET @count = @count+1;

    END;
	RETURN @days;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[retornarDiasRango]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[retornarDiasRango] (
@FechaI Varchar(10),
@FechaF Varchar(10),
@idUser Int)
RETURNS VARCHAR(200)
AS 
BEGIN
	DECLARE @datetimeI datetime = (SELECT CAST(@FechaI AS date));
	DECLARE @datetimeF datetime = (SELECT CAST(@FechaF AS date));

	DECLARE @count INT = 0;
	DECLARE @finalDay INT = (SELECT DATEDIFF(DAY, @datetimeI,@datetimeF));
	
	DECLARE @fechaParam DATETIME = CONVERT(DATE,@datetimeI);
    DECLARE @days VARCHAR(200) = ''; 
    WHILE(@count <= @finalDay)
		BEGIN
			
			IF EXISTS(SELECT * FROM REPORTE WHERE CONVERT(DATE,Rep_FechaRealizado) = @fechaParam AND Rep_User_Id = @idUser) 
			BEGIN
				SET @days = CONCAT(@days,@count,'|');   
			END
			ELSE
			BEGIN
				IF EXISTS(SELECT * FROM REPORTECOVID WHERE CONVERT(DATE,REPC_Fecha) = @fechaParam AND REPC_User_Id = @idUser)
				BEGIN
					SET @days = CONCAT(@days,@count,'|');   
				END
				
			END
			SET @count = @count+1;
			SET @fechaParam = (SELECT DATEADD(day,1,@fechaParam));
			CONTINUE;
    END;
	RETURN @days;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[retornarEstado]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[retornarEstado] 
(
	@idUser INT,
	@FechaReporte DATE
)
RETURNS SMALLINT
AS
BEGIN
	DECLARE @idRep int = (SELECT Rep_Id FROM REPORTE WHERE CONVERT(DATE,Rep_FechaRealizado) = @FechaReporte AND Rep_User_Id = @idUser);
	DECLARE @response SMALLINT;
	DECLARE @hasCovid TINYINT = (SELECT User_HasCovid FROM UsuarioIDE WHERE User_Id = @idUser);
	DECLARE @positivo TINYINT = (SELECT Rep_Positivo FROM REPORTE WHERE CONVERT(DATE,Rep_FechaRealizado) = @FechaReporte AND Rep_User_Id = @idUser);
	DECLARE @preguntasSos TINYINT = (SELECT (Rep_ContactoConfirmado+Rep_ContactoSospechozo) FROM REPORTE WHERE CONVERT(DATE,Rep_FechaRealizado) = @FechaReporte AND Rep_User_Id = @idUser);
	DECLARE @cantSint TINYINT =  (SELECT COUNT(*) FROM REP_SINT_TRANSC WHERE TRANSC_Sint_id <> 13 AND TRANSC_Rep_id = @idRep);
	IF @hasCovid = 1
	BEGIN
		SET @response = 2;
	END
	ELSE
	BEGIN
		IF @positivo = 1
			BEGIN
				set @response = 2;
			END;
		ELSE
			BEGIN
				IF @preguntasSos > 0
					BEGIN
						SET @response = 1;
					END;
				ELSE
					BEGIN
						IF @cantSint > 1
							BEGIN
								SET @response = 1;
							END;
						ELSE
							BEGIN
								SET @response = 3;
							END;
					END;
			END;
	END;
	RETURN @response;
END;
GO
/****** Object:  View [dbo].[vwRandom]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwRandom]
 AS
 SELECT RAND() as Rnd

GO
/****** Object:  StoredProcedure [dbo].[asignarIdApp]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[asignarIdApp] @id INT, @appId VARCHAR(200)
AS
BEGIN
	UPDATE UsuarioIDE 
	SET AppId =@appId
	WHERE User_Id = @id;
END;
GO
/****** Object:  StoredProcedure [dbo].[retornarLastVersion]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[retornarLastVersion]
as 
begin
	select top 1 * from version order by version desc;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_changeEstatusCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_changeEstatusCovid] @userId INT, @hascovid TINYINT
AS
BEGIN
	BEGIN
		IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @userId)
		BEGIN
			UPDATE UsuarioIDE
			SET User_HasCovid = @hascovid
			WHERE User_Id = @userId;
			SELECT '200' AS CODIGO, 'EXITOSO' AS MENSAJE;
		END
		ELSE
		BEGIN
			SELECT '404' AS CODIGO, 'NO EXISTE' AS MENSAJE
		END
	END
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_changePassFromTemp]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_changePassFromTemp]
@id SMALLINT,
@passTemp VARCHAR(150),
@passNew VARCHAR(150)
AS 
BEGIN
    DECLARE @passTempHash VARCHAR(150) = (SELECT HASHBYTES('MD5',@passTemp));
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @id AND User_clave_temp = @passTempHash)
    BEGIN
            DECLARE @passNewHash VARCHAR(150) = (SELECT HASHBYTES('MD5',@passNew));
            UPDATE UsuarioIDE
            SET User_Contrasenia = @passNewHash, User_clave_temp = null
            WHERE User_Id = @id;
            SELECT 'Cambio de contrase�a correcto.' AS Mensaje, 200 Codigo;
    END;
    ELSE
    BEGIN
            SELECT 'Contrase�a Incorrecta.' AS Mensaje, 404 Codigo;
    END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_deshabilitarUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_deshabilitarUser]
(
    @id smallint
)
AS
BEGIN
    IF exists(SELECT * FROM UsuarioIDE WHERE User_Id = @id)
    BEGIN
        UPDATE UsuarioIDE SET User_Estado = 2 WHERE User_Id = @id;
        SELECT 200 AS Codigo, 'Usuario Deshabilitado.' AS Mensaje;
    END;
    ELSE
    BEGIN
        SELECT 404 AS Codigo, 'Usuario No Existe..' AS Mensaje;
    END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_editarClaveUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_editarClaveUsuario] 
@id SMALLINT,
@claveAnterior VARCHAR(150),
@claveNueva VARCHAR(150) 
AS
BEGIN
    DECLARE @clavePast VARCHAR(150) = (SELECT HASHBYTES('MD5',@claveAnterior)); 
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @id AND User_Contrasenia = @clavePast) 
        BEGIN
            DECLARE @clavePost VARCHAR(150) = (SELECT HASHBYTES('MD5',@claveNueva)); 
            UPDATE UsuarioIDE
            SET User_Contrasenia = @clavePost
            WHERE User_Id = @id;
            SELECT 'Actualizaci�n exitosa.' AS Mensaje, 200 AS Codigo;
        END
    ELSE
        BEGIN
            SELECT 'Contrase�a Incorrecta.' AS Mensaje, 404 AS Codigo;
        END;
END;        
GO
/****** Object:  StoredProcedure [dbo].[sp_editarUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_editarUsuario] 
@id SMALLINT,
@nombres VARCHAR(150),
@apellidos VARCHAR(150),
@correo VARCHAR(150),
@telefono VARCHAR(9),
@direccion VARCHAR(200)
AS
BEGIN
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @id) 
        BEGIN
            UPDATE UsuarioIDE 
            SET User_Nombres = @nombres, User_Apellidos = @apellidos,
            User_Correo = @correo, User_Telefono = @telefono,
            User_Direccion = @direccion
            WHERE User_Id = @id;
            SELECT 'Actualizaci�n exitosa.' AS Mensaje, 200 AS Codigo;
        END
    ELSE
        BEGIN
            SELECT 'No se encontr� usuario' AS Mensaje, 404 AS Codigo;
        END;
END;        
GO
/****** Object:  StoredProcedure [dbo].[sp_editarUsuarioAdmin]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_editarUsuarioAdmin] 
@id SMALLINT,
@dni VARCHAR(8),
@nombres VARCHAR(150),
@apellidos VARCHAR(150),
@correo VARCHAR(150),
@telefono VARCHAR(9),
@direccion VARCHAR(200),
@perfil SMALLINT
AS
BEGIN
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @id) 
        BEGIN
			IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Dni = @dni AND User_Id <>@id)
				BEGIN 
					SELECT 'DNI Duplicado' AS Mensaje, 404 AS Codigo;
				END;
			ELSE
				BEGIN
					UPDATE UsuarioIDE 
					SET User_Dni = @dni, User_Nombres = @nombres, User_Apellidos = @apellidos,
					User_Correo = @correo, User_Telefono = @telefono,
					User_Direccion = @direccion,User_Per_Id = @perfil
					WHERE User_Id = @id;
					SELECT 'Actualizaci�n exitosa.' AS Mensaje, 200 AS Codigo;
				END;
        END
    ELSE
        BEGIN
            SELECT 'No se encontr� usuario' AS Mensaje, 404 AS Codigo;
        END;
END;   
GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarRegistrosNowByDNI]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_eliminarRegistrosNowByDNI] @userId INT
AS
BEGIN
	DECLARE @fechaPer DATETIME;
    SET @fechaPer = DATEADD(HH, -5, GETDATE()); 
	print @fechaPer;
	print @userId;
	IF (EXISTS(Select Rep_Id from REPORTE WHERE  YEAR(Rep_FechaRealizado) = YEAR(@fechaPer) AND
									MONTH(Rep_FechaRealizado) = MONTH(@fechaPer)
									AND DAY(Rep_FechaRealizado) = DAY(@fechaPer) AND Rep_User_Id = @userId))
	BEGIN
		DECLARE @reporteId int = (Select Rep_Id from REPORTE 
									WHERE YEAR(Rep_FechaRealizado) = YEAR(@fechaPer) AND
									MONTH(Rep_FechaRealizado) = MONTH(@fechaPer)
									AND DAY(Rep_FechaRealizado) = DAY(@fechaPer)
									AND Rep_User_Id = @userId);
		print @reporteId;
		DELETE FROM REP_SINT_TRANSC WHERE TRANSC_Rep_id = @reporteId;
		DELETE FROM REPORTE WHERE Rep_Id = @reporteId;
	END
	ELSE
	BEGIN
		DECLARE @reporteCovId int = (Select REPC_Id from REPORTECOVID WHERE YEAR(REPC_Fecha) = YEAR(@fechaPer) AND
									MONTH(REPC_Fecha) = MONTH(@fechaPer)
									AND DAY(REPC_Fecha) = DAY(@fechaPer)
									AND REPC_User_Id = @userId);
	
		DELETE FROM REPC_SIG_TRANS WHERE TRANSC_REPC_ID = @reporteCovId;
		DELETE FROM REPC_SINT_TRANS WHERE TRANSSIN_REPC_ID = @reporteCovId;
		DELETE FROM REPORTECOVID WHERE REPC_Id = @reporteCovId;
	END
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_eliminarUsuario]
@id SMALLINT
AS
BEGIN
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Id = @id)
    BEGIN
        UPDATE UsuarioIDE
        SET User_Estado = 1
        WHERE User_Id = @id;
        SELECT 'Usuario Eliminado.' AS Mensaje, 200 as Codigo;
    END;
    ELSE
    BEGIN 
        SELECT 'Usuario No Encontrado.' AS Mensaje, 404 as Codigo;
    END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_ForgotPassword]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ForgotPassword] 
@correo VARCHAR(200),
@dni VARCHAR(10)
AS
BEGIN
    IF EXISTS(SELECT * FROM UsuarioIDE WHERE User_Correo = @correo AND User_Dni = @dni)
    BEGIN
        DECLARE @pass VARCHAR(150) = (SELECT dbo.fnCustomPass(8,'CN'));
        DECLARE @passHash VARCHAR(150)= (SELECT HASHBYTES('MD5',@pass));
        UPDATE UsuarioIDE
        SET User_clave_temp = @passHash
        WHERE User_Correo = @correo and User_Dni = @dni;
        SELECT 'Usuario Identificado. Revise su correo.' AS Mensaje, 200 as Codigo, @pass AS Contra;
    END;
    ELSE
    BEGIN
        SELECT 'Usuario NO Identificado' AS Mensaje, 404 as Codigo, 'No Contrase�a' AS Contra;
    END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_habilitarUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_habilitarUser]
(
    @id smallint
)
AS
BEGIN
    IF exists(SELECT * FROM UsuarioIDE WHERE User_Id = @id)
    BEGIN
        UPDATE UsuarioIDE SET User_Estado = 0 WHERE User_Id = @id;
        SELECT 200 AS Codigo, 'Usuario Deshabilitado.' AS Mensaje;
    END;
    ELSE
    BEGIN
        SELECT 404 AS Codigo, 'Usuario No Existe..' AS Mensaje;
    END;
END;

GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_IngresarReporte] 
@sospechozo TINYINT,
@confirmado TINYINT,
@positivo TINYINT,
@cantDosis TINYINT,
@userId SMALLINT,
@fechaPer DATETIME
AS
BEGIN
    DECLARE @Id Int;
	IF (dbo.fn_ValidarFechaAndUser(@fechaPer,@userId)) = 1
	BEGIN
		INSERT INTO dbo.REPORTE (Rep_FechaRealizado,Rep_ContactoSospechozo,Rep_ContactoConfirmado,
		Rep_Positivo,Rep_User_Id,Rep_CantidadDosis)
		VALUES (@fechaPer,@sospechozo,@confirmado,@positivo,@userId,@cantDosis);
		SET @Id = SCOPE_IDENTITY();
		SELECT Rep_Id FROM dbo.REPORTE WHERE Rep_Id = @Id;
	END
	ELSE
	BEGIN
		SELECT 0 AS Id;
	END
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_ingresarReporteCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ingresarReporteCovid] @userId INT, @fechaPer DATETIME,@temp DECIMAL(5,2),@comentario VARCHAR(255)  
AS 
BEGIN
    DECLARE @Id Int;
	IF (dbo.fn_ValidarFechaAndUser(@fechaPer,@userId)) = 1
	BEGIN
		INSERT INTO dbo.REPORTECOVID (REPC_Fecha,REPC_Temperatura,REPC_Comentario,REPC_User_Id)
		VALUES (@fechaPer,@temp,@comentario,@userId);
		SET @Id = SCOPE_IDENTITY();
		SELECT REPC_Id FROM dbo.REPORTECOVID WHERE REPC_Id = @Id;
	END
	ELSE
	BEGIN
		SELECT 0 AS Id;
	END    
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarSubReportes]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_IngresarSubReportes]
@Reporte_ID INT,
@Sintoma_ID TINYINT,
@OtroDetalle VARCHAR(200)
AS
BEGIN

    BEGIN TRY  
        INSERT INTO dbo.REP_SINT_TRANSC (TRANSC_Sint_id,TRANSC_Rep_id,TRANSC_Otro) 
        VALUES (@Sintoma_ID,@Reporte_ID,@OtroDetalle);
        SELECT '200' AS CODIGO,'CORRECTO' AS MENSAJE ;
    END TRY 
    BEGIN CATCH  
        SELECT '400' AS CODIGO,ERROR_MESSAGE() AS MENSAJE ;
    END CATCH 
END;

GO
/****** Object:  StoredProcedure [dbo].[sp_ingresarSubReporteSignos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ingresarSubReporteSignos] 
@reporteId INT, 
@SigId SMALLINT, 
@Otro VARCHAR(200)
AS 
BEGIN
BEGIN TRY  
        INSERT INTO REPC_SIG_TRANS (TRANSC_REPC_ID,TRANSC_SIG_ID,TRANSC_OTRODETALLE) 
        VALUES (@reporteID,@SigId,@Otro);
        SELECT '200' AS CODIGO,'CORRECTO' AS MENSAJE ;
    END TRY 
    BEGIN CATCH  
        SELECT '400' AS CODIGO,ERROR_MESSAGE() AS MENSAJE ;
    END CATCH 
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarSubReportesSintomasCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_IngresarSubReportesSintomasCovid]
@Reporte_ID INT,
@Sintoma_ID TINYINT,
@OtroDetalle VARCHAR(200)
AS
BEGIN
	BEGIN TRY  
        INSERT INTO dbo.REPC_SINT_TRANS(TRANSSIN_REPC_ID,TRANSSIN_SIN_ID,TRANSSIN_OTRODETALLE) 
        VALUES (@Reporte_ID,@Sintoma_ID,@OtroDetalle);
        SELECT '200' AS CODIGO,'CORRECTO' AS MENSAJE ;
    END TRY 
    BEGIN CATCH  
        SELECT '400' AS CODIGO,ERROR_MESSAGE() AS MENSAJE ;
    END CATCH 
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_insertarError]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_insertarError] (@operacion VARCHAR(250),@tblRef VARCHAR(250),@userId Int)
AS
BEGIN
	DECLARE @fechaPer DATETIME;
    SET @fechaPer = DATEADD(HH, -5, GETDATE()); 
	INSERT INTO Logs (FECHALog, OPERACION,TABLAREF,USUARIO)
	VALUES (@fechaPer,@operacion,@tblRef,@userId)
END;


GO
/****** Object:  StoredProcedure [dbo].[sp_insertUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_insertUsuario] @dni VARCHAR(10),@nombre VARCHAR(150),@apellido VARCHAR(150),@telefono VARCHAR(11),@perfilId INT,
@correo VARCHAR(200), @direccion VARCHAR(200)
AS
BEGIN
    IF EXISTS (SELECT * FROM UsuarioIDE WHERE User_Dni = @dni)
    BEGIN
       SELECT 0 AS User_Id, 404 AS Codigo, 'DNI DUPLICADO' AS Mensaje,'ERROR' AS 'PasswordTemp';
    END;
    ELSE
    BEGIN
     DECLARE @pass VARCHAR(150) = (SELECT dbo.fnCustomPass(8,'CN'));
        DECLARE @passSave VARCHAR(150);
        SET @passSave = (SELECT HASHBYTES('MD5',@pass)); 
        INSERT INTO dbo.UsuarioIDE (User_Dni,User_Nombres,User_Apellidos,User_Telefono,User_clave_temp,User_Per_Id,User_Correo,User_Direccion)
        VALUES (@dni,@nombre,@apellido,@telefono,@passSave,@perfilId,@correo,@direccion);
        SELECT TOP 1 User_Id, 200 AS Codigo, CONCAT('Bienvenido usuario ', @nombre) AS Mensaje,@pass AS 'PasswordTemp' FROM UsuarioIDE ORDER BY  User_Id DESC;
        
    END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listaDeReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listaDeReporte]
AS
BEGIN
select rep.Rep_Id AS 'ID DE REPORTE',
rep.Rep_FechaRealizado AS 'FECHA DE REPORTE',
rep.Rep_CantidadDosis AS 'CANTIDAD DE DOSIS',
CASE rep.Rep_ContactoConfirmado
	WHEN 1 THEN 'Si'
	WHEN 0 THEN 'No'
END AS 'TUVO CONTACTO CON UN POSITIVO',
CASE rep.Rep_ContactoSospechozo
	WHEN 1 THEN 'Si'
	WHEN 0 THEN 'No'
END AS 'TUVO CONTACTO CON SOSPECHOSO',
CASE rep.Rep_Positivo
	WHEN 1 THEN 'Si'
	WHEN 0 THEN 'No'
END AS 'DIO POSITIVO',
sint.Sint_Detalle AS 'SINTOMA',
trans.TRANSC_Otro AS 'OTRO SINTOMA'
from REPORTE AS rep INNER JOIN REP_SINT_TRANSC AS trans ON rep.Rep_Id=trans.TRANSC_Rep_id
INNER JOIN SINTOMAS as sint ON trans.TRANSC_Sint_id = sint.Sint_Id
WHERE Rep_User_Id NOT IN (SELECT User_Id FROM UsuarioIDE where User_Estado NOT IN (0,2));
END;


GO
/****** Object:  StoredProcedure [dbo].[sp_listarParaDiaExcel]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarParaDiaExcel] @dia INT, @mes INT, @anio INT
AS
BEGIN

	DECLARE @count INT;
	DECLARE @userId INT;
	CREATE TABLE #Usuarios(
		Id_usuario INT
	);

	INSERT INTO #Usuarios 
	SELECT User_Id FROM UsuarioIDE;

	SELECT @count = COUNT(*) FROM #Usuarios;

	CREATE TABLE #Reporte
	(
	IdUser INT,Nombres VARCHAR(100),Apellidos VARCHAR(100), Dni VARCHAR(8), Telefono VARCHAR(9),
	Correo VARCHAR(150),Direccion VARCHAR(150),Cargo VARCHAR(20),IdRep INT,FechaRep DATETIME,CantDosis SMALLINT,
	Confirmado TINYINT, Sospechozo TINYINT, Positivo TINYINT, Sintomas VARCHAR(150), Signos VARCHAR(150),Temp DECIMAL(5,2),
	Comentario VARCHAR(255),Normales Tinyint
	)

	WHILE @count > 0
	BEGIN
		SET @userId = (SELECT TOP 1 Id_usuario FROM #Usuarios);
		IF EXISTS(SELECT * FROM REPORTE WHERE Rep_User_Id=@userId AND year(Rep_FechaRealizado)=@anio AND month(Rep_FechaRealizado)=@mes AND day(Rep_FechaRealizado)=@dia)
		BEGIN
			INSERT INTO #Reporte (IdUser,Nombres,Apellidos,Dni,Telefono,Correo,Direccion,Cargo,IdRep,FechaRep,CantDosis,Confirmado,Sospechozo,Positivo,Sintomas,Normales)
			select 
			-- Datos de Persona
			DISTINCT(users.User_Id) AS IdUser,
			users.User_Nombres AS Nombres,
			users.User_Apellidos AS Apellidos,
			users.User_Dni AS Dni,
			users.User_Telefono AS Telefono,
			users.User_Correo AS Correo,
			users.User_Direccion AS Direccion,
			per.Per_Detalle AS Perfils,
			-- Datos de Reporte
			rep.Rep_Id AS Id,
			rep.Rep_FechaRealizado AS FechaRep,
			rep.Rep_CantidadDosis AS CantDosis,
			CAST(rep.Rep_ContactoConfirmado AS bit) AS Confirmado,
			CAST(rep.Rep_ContactoSospechozo AS bit) AS Sospechozo,
			CAST(rep.Rep_Positivo AS bit) AS Positivo,
			-- Datos de Sintoma
			(SELECT dbo.fn_RetornarSintomasNormal (rep.Rep_Id)) AS Sintomas,
			CAST(1 AS bit) AS Normales
			from UsuarioIDE AS users INNER JOIN Perfil AS per ON users.User_Per_Id = per.Per_Id
			INNER JOIN REPORTE AS rep ON users.User_Id = rep.Rep_User_Id
			INNER JOIN REP_SINT_TRANSC AS trans ON rep.Rep_Id=trans.TRANSC_Rep_id
			INNER JOIN SINTOMAS as sint ON trans.TRANSC_Sint_id = sint.Sint_Id
			WHERE Rep_User_Id = @userId AND DAY(Rep_FechaRealizado) = @dia AND MONTH(Rep_FechaRealizado) = @mes AND YEAR(Rep_FechaRealizado) = @anio;
		END;
		ELSE
		BEGIN
			INSERT INTO #Reporte (IdUser,Nombres,Apellidos,Dni,Telefono,Correo,Direccion,Cargo,IdRep,FechaRep,Temp,Comentario,Sintomas,Signos,Normales)
			--DATOS DE USUARIO
			SELECT DISTINCT(users.User_Id) AS IdUser,
			users.User_Nombres AS Nombres,
			users.User_Apellidos AS Apellidos,
			users.User_Dni AS Dni,
			users.User_Telefono AS Telefono,
			users.User_Correo AS Correo,
			users.User_Direccion AS Direccion,
			per.Per_Detalle AS Perfils,
			--DATOS DE REPORTE
			REP.REPC_Id AS Id,
			rep.REPC_Fecha AS FechaRep,
			rep.REPC_Temperatura AS Temp,
			rep.REPC_Comentario AS Comentario,
			(SELECT dbo.fn_RetornarSintomasCovid (rep.REPC_Id)) AS Sintomas,
			(SELECT dbo.fn_RetornarSignosCovid (rep.REPC_Id)) AS Signos,
			CAST(0 AS bit) AS Normales
			FROM UsuarioIDE AS users INNER JOIN Perfil AS per ON users.User_Per_Id = per.Per_Id
			INNER JOIN REPORTECOVID AS rep ON users.User_Id = rep.REPC_User_Id
			INNER JOIN REPC_SINT_TRANS AS trans ON rep.REPC_Id=trans.TRANSSIN_REPC_ID
			INNER JOIN SINTOMAS as sint ON trans.TRANSSIN_SIN_ID = sint.Sint_Id 
			INNER JOIN REPC_SIG_TRANS AS sig ON rep.REPC_Id = sig.TRANSC_REPC_ID
			INNER JOIN SIGNOS AS Signo ON sig.TRANSC_SIG_ID = Signo.Sig_Id
			WHERE REPC_User_Id=@userId AND year(REPC_Fecha)=@anio AND month(REPC_Fecha)=@mes AND day(REPC_Fecha)=@dia
		END;
		DELETE TOP (1) FROM #Usuarios;
		SELECT @count = COUNT(*) FROM #Usuarios;
	END;

	
END;
SELECT * FROM #Reporte;
	DROP TABLE #Usuarios;
	DROP TABLE #Reporte;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarPersonaDiaReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarPersonaDiaReporte] @userId INT, @dia INT, @mes INT, @anio INT
AS
BEGIN
	IF EXISTS(SELECT * FROM REPORTE WHERE Rep_User_Id=@userId AND year(Rep_FechaRealizado)=@anio AND month(Rep_FechaRealizado)=@mes AND day(Rep_FechaRealizado)=@dia)
	BEGIN
		select 
		-- Datos de Persona
		DISTINCT(users.User_Id) AS IdUser,
		users.User_Nombres AS Nombres,
		users.User_Apellidos AS Apellidos,
		users.User_Dni AS Dni,
		users.User_Telefono AS Telefono,
		users.User_Correo AS Correo,
		users.User_Direccion AS Direccion,
		per.Per_Detalle AS Perfils,
		-- Datos de Reporte
		rep.Rep_Id AS Id,
		rep.Rep_FechaRealizado AS FechaRep,
		rep.Rep_CantidadDosis AS CantDosis,
		CAST(rep.Rep_ContactoConfirmado AS bit) AS Confirmado,
		CAST(rep.Rep_ContactoSospechozo AS bit) AS Sospechozo,
		CAST(rep.Rep_Positivo AS bit) AS Positivo,
		-- Datos de Sintoma
		(SELECT dbo.fn_RetornarSintomasNormal (rep.Rep_Id)) AS Sintomas,
		CAST(1 AS bit) AS Normal
		from UsuarioIDE AS users INNER JOIN Perfil AS per ON users.User_Per_Id = per.Per_Id
		INNER JOIN	 REPORTE AS rep ON users.User_Id = rep.Rep_User_Id
		INNER JOIN REP_SINT_TRANSC AS trans ON rep.Rep_Id=trans.TRANSC_Rep_id
		INNER JOIN SINTOMAS as sint ON trans.TRANSC_Sint_id = sint.Sint_Id
		WHERE Rep_User_Id = @userId AND DAY(Rep_FechaRealizado) = @dia AND MONTH(Rep_FechaRealizado) = @mes AND YEAR(Rep_FechaRealizado) = @anio;
	END;
	ELSE
	BEGIN
		--DATOS DE USUARIO
		SELECT DISTINCT(users.User_Id) AS IdUser,
		users.User_Nombres AS Nombres,
		users.User_Apellidos AS Apellidos,
		users.User_Dni AS Dni,
		users.User_Telefono AS Telefono,
		users.User_Correo AS Correo,
		users.User_Direccion AS Direccion,
		per.Per_Detalle AS Perfils,
		--DATOS DE REPORTE
		REP.REPC_Id AS Id,
		rep.REPC_Fecha AS FechaRep,
		rep.REPC_Temperatura AS Temp,
		rep.REPC_Comentario AS Comentario,
		(SELECT dbo.fn_RetornarSintomasCovid (rep.REPC_Id)) AS Sintomas,
		(SELECT dbo.fn_RetornarSignosCovid (rep.REPC_Id)) AS Signos,
		CAST(0 AS bit) AS Normal
		FROM UsuarioIDE AS users INNER JOIN Perfil AS per ON users.User_Per_Id = per.Per_Id
		INNER JOIN REPORTECOVID AS rep ON users.User_Id = rep.REPC_User_Id
		INNER JOIN REPC_SINT_TRANS AS trans ON rep.REPC_Id=trans.TRANSSIN_REPC_ID
		INNER JOIN SINTOMAS as sint ON trans.TRANSSIN_SIN_ID = sint.Sint_Id 
		INNER JOIN REPC_SIG_TRANS AS sig ON rep.REPC_Id = sig.TRANSC_REPC_ID
		INNER JOIN SIGNOS AS Signo ON sig.TRANSC_SIG_ID = Signo.Sig_Id
		WHERE REPC_User_Id=@userId AND year(REPC_Fecha)=@anio AND month(REPC_Fecha)=@mes AND day(REPC_Fecha)=@dia
	END;
	
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarRangos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarRangos] 
 (
@FechaI Varchar(10),
@FechaF Varchar(10))
AS
BEGIN
	CREATE TABLE #temp_reporteXPeriodo(
		Dni VARCHAR(10) NOT NULL,
		Nombre VARCHAR(255) NOT NULL,
		TipoTrabajador VARCHAR(20) NOT NULL,
		Dias VARCHAR(200) NOT NULL
	);
	INSERT INTO #temp_reporteXPeriodo
	SELECT DISTINCT(users.User_Dni) AS Dni, 
    CONCAT(users.User_Nombres,' ',users.User_Apellidos) AS 'Nombre', 
    perf.Per_Detalle AS 'TipoTra',
	(SELECT dbo.retornarDiasRango(@FechaI,@FechaF,users.User_Id)) AS 'Dias'
    FROM dbo.UsuarioIDE AS users INNER JOIN Perfil AS perf ON users.User_Per_Id = perf.Per_Id	;

	SELECT * FROM #temp_reporteXPeriodo ORDER BY Dni ASC;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarReporteXDia]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarReporteXDia] @dia INT, @mes INT, @anio INT
AS
BEGIN

	CREATE TABLE #tb_temp_Id_Estado(
		Id INT NOT NULL,
		Estado SMALLINT NOT NULL DEFAULT 0,
		Id_rep INT NOT NULL
	)
	INSERT INTO #tb_temp_Id_Estado
	SELECT Rep_User_Id,
	dbo.retornarEstado(Rep_User_Id,CAST(CONCAT(@anio,'-',@mes,'-',@dia) AS DATE)),
	Rep_Id
	FROM REPORTE 
	WHERE DAY(Rep_FechaRealizado) = @dia AND MONTH(Rep_FechaRealizado) = @mes AND YEAR(Rep_FechaRealizado) = @anio;
	INSERT INTO #tb_temp_Id_Estado
	SELECT REPC_User_Id,
	dbo.retornarEstado(REPC_User_Id,CAST(CONCAT(@anio,'-',@mes,'-',@dia) AS DATE)),
	REPC_Id
	FROM REPORTECOVID 
	WHERE DAY(REPC_Fecha) = @dia AND MONTH(REPC_Fecha) = @mes AND YEAR(REPC_Fecha) = @anio;

	CREATE TABLE #Ids(
		Id INT NOT NULL,
		Estado SMALLINT NOT NULL DEFAULT 0,
		Descripcion VARCHAR(20)
	)
	INSERT INTO #Ids
	SELECT Id,Estado,dbo.responseDescripcionDia(Estado,Id_rep) FROM #tb_temp_Id_Estado;


	SELECT User_Id AS Id,
	User_Dni AS Dni, 
	CONCAT(User_Nombres,' ',User_Apellidos) AS Nombre,
	per.Per_Detalle AS Perfils,
	ISNULL(temp.Estado,0) AS 'Status',
	temp.Descripcion AS Descripcion,
	users.AppId AS 'AppId'
	FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id
	left JOIN #Ids as temp ON temp.Id = users.User_Id ORDER by User_Dni;
END;

GO
/****** Object:  StoredProcedure [dbo].[sp_listarReporteXPeriodo]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarReporteXPeriodo] @mes INT, @anio INT  
AS
BEGIN
	CREATE TABLE #temp_reporteXPeriodo(
		Dni VARCHAR(10) NOT NULL,
		Nombre VARCHAR(255) NOT NULL,
		TipoTrabajador VARCHAR(20) NOT NULL,
		Dias VARCHAR(200) NOT NULL
	);
	INSERT INTO #temp_reporteXPeriodo
	SELECT DISTINCT(users.User_Dni) AS Dni, 
    CONCAT(users.User_Nombres,' ',users.User_Apellidos) AS 'Nombre', 
    perf.Per_Detalle AS 'TipoTra',
	(SELECT dbo.retornarDias(users.User_Id,@mes,@anio))
    FROM dbo.UsuarioIDE AS users INNER JOIN Perfil AS perf ON users.User_Per_Id = perf.Per_Id	;

	SELECT * FROM #temp_reporteXPeriodo ORDER BY Dni ASC;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarSignos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarSignos]
AS BEGIN
SELECT * FROM SIGNOS;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarSintomas]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarSintomas]
AS
BEGIN
    SELECT * FROM dbo.SINTOMAS;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarTiposTrabajador]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarTiposTrabajador]
AS
BEGIN
	SELECT * FROM Perfil;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarUsuarioCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarUsuarioCovid] 
AS
BEGIN
	
	SELECT User_Id AS Id,
	User_Dni AS Dni, 
	CONCAT(User_Nombres,' ',User_Apellidos) AS Nombre,
	per.Per_Detalle AS Perfils,
	CAST(Users.User_HasCovid AS bit) as HasCovid
	FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id
	Order by  User_Dni; 
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_listarUsuarios]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_listarUsuarios]
AS 
BEGIN
    SELECT User_Id,User_Dni,User_Nombres,
    User_Apellidos,User_Correo,
    User_Telefono,User_Direccion,
    (SELECT Per_Detalle FROM Perfil WHERE Per_Id = User_Per_Id) AS User_Per_Id,
	(SELECT con_descripcion FROM CONCEPTOS WHERE con_correlativo = User_Estado AND con_prefijo = 1) AS Estado
    FROM UsuarioIDE
    WHERE User_Estado <> 1;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_LoginAndValidCompleted]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LoginAndValidCompleted] @dni VARCHAR(10),@pass VARCHAR(150)  
AS
BEGIN
	DECLARE @IdComparar SMALLINT;
	DECLARE @clave VARCHAR(150) = (SELECT HASHBYTES('MD5',@pass)); 
	print @pass;
	Print @clave;
	SET @IdComparar = (SELECT User_Id FROM UsuarioIDE  WHERE User_Dni like @dni);
	DECLARE @fechaUltimoRep DATETIME;
	DECLARE @fechaActual DATETIME = DATEADD(HH, -5, GETDATE()); 
	SET @fechaUltimoRep = (SELECT TOP 1 Rep_FechaRealizado FROM dbo.REPORTE WHERE Rep_User_Id = @IdComparar ORDER BY Rep_FechaRealizado DESC);
	IF EXISTS(SELECT * FROM UsuarioIDE  WHERE User_Dni like @dni AND User_Contrasenia = @clave AND User_Estado <> 1)
	BEGIN
		IF (YEAR(@fechaUltimoRep)=YEAR(@fechaActual) AND MONTH(@fechaUltimoRep)=MONTH(@fechaActual) AND DAY(@fechaUltimoRep)=DAY(@fechaActual))
		BEGIN
			SELECT User_Id,User_Dni,200 as Codigo, User_Nombres,User_Apellidos, CAST(1 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,
			CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
			FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
		END;
		ELSE
		BEGIN
			SET @fechaUltimoRep = (SELECT TOP 1 REPC_Fecha FROM dbo.REPORTECOVID WHERE REPC_User_Id = @IdComparar ORDER BY REPC_Fecha DESC); 
			IF (YEAR(@fechaUltimoRep)=YEAR(@fechaActual) AND MONTH(@fechaUltimoRep)=MONTH(@fechaActual) AND DAY(@fechaUltimoRep)=DAY(@fechaActual)) 
			BEGIN
				SELECT User_Id,User_Dni, 200 as Codigo,User_Nombres,User_Apellidos, CAST(1 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
				FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
			END
			ELSE
			BEGIN
				SELECT User_Id,User_Dni, 200 as Codigo,User_Nombres,User_Apellidos, CAST(0 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
				FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
			END
		END;
	END;
	ELSE IF EXISTS(SELECT * FROM UsuarioIDE  WHERE User_Dni like @dni AND User_clave_temp = @clave AND User_Estado <> 1)
	BEGIN
		IF (YEAR(@fechaUltimoRep)=YEAR(@fechaActual) AND MONTH(@fechaUltimoRep)=MONTH(@fechaActual) AND DAY(@fechaUltimoRep)=DAY(@fechaActual))
		BEGIN
			SELECT User_Id,User_Dni,205 as Codigo, User_Nombres,User_Apellidos, CAST(1 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,
			CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
			FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
		END;
		ELSE
		BEGIN
			SET @fechaUltimoRep = (SELECT TOP 1 REPC_Fecha FROM dbo.REPORTECOVID WHERE REPC_User_Id = @IdComparar ORDER BY REPC_Fecha DESC); 
			IF (YEAR(@fechaUltimoRep)=YEAR(@fechaActual) AND MONTH(@fechaUltimoRep)=MONTH(@fechaActual) AND DAY(@fechaUltimoRep)=DAY(@fechaActual)) 
			BEGIN
				SELECT User_Id,User_Dni, 205 as Codigo,User_Nombres,User_Apellidos, CAST(1 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
				FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
			END
			ELSE
			BEGIN
				SELECT User_Id,User_Dni, 205 as Codigo,User_Nombres,User_Apellidos, CAST(0 as bit) AS Completado,CAST(User_HasCovid AS bit) AS HasCovid,CAST(1 as bit) AS Logueado, per.Per_Detalle AS Perfils,User_Estado AS Estado
				FROM dbo.UsuarioIDE AS users INNER JOIN Perfil as per ON users.User_Per_Id = per.Per_Id WHERE User_Dni like  @dni;
			END
		END;
	END;
	ELSE
	BEGIN 
		SELECT 404 AS Codigo, CAST(0 AS bit) AS Logueado;
	END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_verUsuarioXId]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_verUsuarioXId] @id INT
AS
BEGIN
    SELECT User_Id,User_Dni,User_Nombres,
    User_Apellidos,User_Correo,
    User_Telefono,User_Direccion,
    User_Per_Id
    FROM UsuarioIDE
    WHERE User_Id = @id;
END;
GO
/****** Object:  StoredProcedure [dbo].[traerCuerpoPush]    Script Date: 26/05/2022 09:54:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traerCuerpoPush] 
AS
BEGIN
	SELECT TOP 1 cabecera,contenido FROM Mensaje ORDER BY Id DESC;
END;
GO
