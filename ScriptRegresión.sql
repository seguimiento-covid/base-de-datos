/****** Object:  StoredProcedure [dbo].[traerCuerpoPush]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[traerCuerpoPush]
GO
/****** Object:  StoredProcedure [dbo].[sp_verUsuarioXId]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_verUsuarioXId]
GO
/****** Object:  StoredProcedure [dbo].[sp_LoginAndValidCompleted]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_LoginAndValidCompleted]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarUsuarios]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarUsuarios]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarUsuarioCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarUsuarioCovid]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarTiposTrabajador]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarTiposTrabajador]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarSintomas]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarSintomas]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarSignos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarSignos]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarReporteXPeriodo]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarReporteXPeriodo]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarReporteXDia]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarReporteXDia]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarRangos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarRangos]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarPersonaDiaReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarPersonaDiaReporte]
GO
/****** Object:  StoredProcedure [dbo].[sp_listarParaDiaExcel]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listarParaDiaExcel]
GO
/****** Object:  StoredProcedure [dbo].[sp_listaDeReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_listaDeReporte]
GO
/****** Object:  StoredProcedure [dbo].[sp_insertUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_insertUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_insertarError]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_insertarError]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarSubReportesSintomasCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_IngresarSubReportesSintomasCovid]
GO
/****** Object:  StoredProcedure [dbo].[sp_ingresarSubReporteSignos]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_ingresarSubReporteSignos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarSubReportes]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_IngresarSubReportes]
GO
/****** Object:  StoredProcedure [dbo].[sp_ingresarReporteCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_ingresarReporteCovid]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngresarReporte]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_IngresarReporte]
GO
/****** Object:  StoredProcedure [dbo].[sp_habilitarUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_habilitarUser]
GO
/****** Object:  StoredProcedure [dbo].[sp_ForgotPassword]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_ForgotPassword]
GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_eliminarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarRegistrosNowByDNI]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_eliminarRegistrosNowByDNI]
GO
/****** Object:  StoredProcedure [dbo].[sp_editarUsuarioAdmin]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_editarUsuarioAdmin]
GO
/****** Object:  StoredProcedure [dbo].[sp_editarUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_editarUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_editarClaveUsuario]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_editarClaveUsuario]
GO
/****** Object:  StoredProcedure [dbo].[sp_deshabilitarUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_deshabilitarUser]
GO
/****** Object:  StoredProcedure [dbo].[sp_changePassFromTemp]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_changePassFromTemp]
GO
/****** Object:  StoredProcedure [dbo].[sp_changeEstatusCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[sp_changeEstatusCovid]
GO
/****** Object:  StoredProcedure [dbo].[retornarLastVersion]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[retornarLastVersion]
GO
/****** Object:  StoredProcedure [dbo].[asignarIdApp]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP PROCEDURE IF EXISTS [dbo].[asignarIdApp]
GO
/****** Object:  View [dbo].[vwRandom]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP VIEW IF EXISTS [dbo].[vwRandom]
GO
/****** Object:  UserDefinedFunction [dbo].[retornarEstado]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[retornarEstado]
GO
/****** Object:  UserDefinedFunction [dbo].[retornarDiasRango]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[retornarDiasRango]
GO
/****** Object:  UserDefinedFunction [dbo].[retornarDias]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[retornarDias]
GO
/****** Object:  UserDefinedFunction [dbo].[responseDescripcionDia]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[responseDescripcionDia]
GO
/****** Object:  UserDefinedFunction [dbo].[fnCustomPass]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[fnCustomPass]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ValidarFechaAndUser]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[fn_ValidarFechaAndUser]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSintomasNormal]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[fn_RetornarSintomasNormal]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSintomasCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[fn_RetornarSintomasCovid]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RetornarSignosCovid]    Script Date: 26/05/2022 09:54:33 p. m. ******/
DROP FUNCTION IF EXISTS [dbo].[fn_RetornarSignosCovid]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioIDE]') AND type in (N'U'))
ALTER TABLE [dbo].[UsuarioIDE] DROP CONSTRAINT IF EXISTS [FK_PersonPerf]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPORTECOVID]') AND type in (N'U'))
ALTER TABLE [dbo].[REPORTECOVID] DROP CONSTRAINT IF EXISTS [FK__REPORTECO__REPC___3493CFA7]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPORTE]') AND type in (N'U'))
ALTER TABLE [dbo].[REPORTE] DROP CONSTRAINT IF EXISTS [FK__REPORTE__Rep_Use__17F790F9]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPC_SINT_TRANS]') AND type in (N'U'))
ALTER TABLE [dbo].[REPC_SINT_TRANS] DROP CONSTRAINT IF EXISTS [FK__REPC_SINT__TRANS__3A4CA8FD]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPC_SINT_TRANS]') AND type in (N'U'))
ALTER TABLE [dbo].[REPC_SINT_TRANS] DROP CONSTRAINT IF EXISTS [FK__REPC_SINT__TRANS__395884C4]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPC_SIG_TRANS]') AND type in (N'U'))
ALTER TABLE [dbo].[REPC_SIG_TRANS] DROP CONSTRAINT IF EXISTS [FK__REPC_SIG___TRANS__3E1D39E1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPC_SIG_TRANS]') AND type in (N'U'))
ALTER TABLE [dbo].[REPC_SIG_TRANS] DROP CONSTRAINT IF EXISTS [FK__REPC_SIG___TRANS__3D2915A8]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REP_SINT_TRANSC]') AND type in (N'U'))
ALTER TABLE [dbo].[REP_SINT_TRANSC] DROP CONSTRAINT IF EXISTS [FK__REP_SINT___TRANS__1DB06A4F]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REP_SINT_TRANSC]') AND type in (N'U'))
ALTER TABLE [dbo].[REP_SINT_TRANSC] DROP CONSTRAINT IF EXISTS [FK__REP_SINT___TRANS__1CBC4616]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioIDE]') AND type in (N'U'))
ALTER TABLE [dbo].[UsuarioIDE] DROP CONSTRAINT IF EXISTS [DF__UsuarioID__User___503BEA1C]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioIDE]') AND type in (N'U'))
ALTER TABLE [dbo].[UsuarioIDE] DROP CONSTRAINT IF EXISTS [DF__UsuarioID__User___4F47C5E3]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioIDE]') AND type in (N'U'))
ALTER TABLE [dbo].[UsuarioIDE] DROP CONSTRAINT IF EXISTS [DF__UsuarioID__User___42E1EEFE]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPORTECOVID]') AND type in (N'U'))
ALTER TABLE [dbo].[REPORTECOVID] DROP CONSTRAINT IF EXISTS [DF__REPORTECO__REPC___47A6A41B]
GO
/****** Object:  Index [Uk_Dni]    Script Date: 02/05/2022 10:56:04 p. m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioIDE]') AND type in (N'U'))
ALTER TABLE [dbo].[UsuarioIDE] DROP CONSTRAINT IF EXISTS [Uk_Dni]
GO
/****** Object:  Table [dbo].[version]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[version]
GO
/****** Object:  Table [dbo].[Mensaje]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[Mensaje]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[Logs]
GO
/****** Object:  Table [dbo].[REPC_SINT_TRANS]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[REPC_SINT_TRANS]
GO
/****** Object:  Table [dbo].[REPC_SIG_TRANS]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[REPC_SIG_TRANS]
GO
/****** Object:  Table [dbo].[REP_SINT_TRANSC]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[REP_SINT_TRANSC]
GO
/****** Object:  Table [dbo].[SINTOMAS]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[SINTOMAS]
GO
/****** Object:  Table [dbo].[SIGNOS]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[SIGNOS]
GO
/****** Object:  Table [dbo].[REPORTECOVID]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[REPORTECOVID]
GO
/****** Object:  Table [dbo].[REPORTE]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[REPORTE]
GO
/****** Object:  Table [dbo].[UsuarioIDE]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[UsuarioIDE]
GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 02/05/2022 10:56:04 p. m. ******/
DROP TABLE IF EXISTS [dbo].[Perfil]
GO
DROP TABLE IF EXISTS [dbo].[CONCEPTOS]
GO
