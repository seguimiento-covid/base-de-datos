/****** Object:  Table [dbo].[CONCEPTOS]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTOS](
	[con_id] [smallint] IDENTITY(1,1) NOT NULL,
	[con_prefijo] [smallint] NOT NULL,
	[con_correlativo] [smallint] NOT NULL,
	[con_descripcion] [varchar](100) NOT NULL,
	[con_estado] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[con_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[FECHALog] [datetime] NOT NULL,
	[OPERACION] [varchar](250) NOT NULL,
	[TABLAREF] [varchar](250) NOT NULL,
	[USUARIO] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mensaje]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mensaje](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[cabecera] [varchar](200) NOT NULL,
	[contenido] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil](
	[Per_Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Per_Detalle] [varchar](20) NULL,
 CONSTRAINT [Pk_Perfil] PRIMARY KEY CLUSTERED 
(
	[Per_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REP_SINT_TRANSC]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REP_SINT_TRANSC](
	[TRANSC_Sint_id] [int] NOT NULL,
	[TRANSC_Rep_id] [int] NOT NULL,
	[TRANSC_Otro] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REPC_SIG_TRANS]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REPC_SIG_TRANS](
	[TRANSC_REPC_ID] [int] NOT NULL,
	[TRANSC_SIG_ID] [smallint] NOT NULL,
	[TRANSC_OTRODETALLE] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REPC_SINT_TRANS]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REPC_SINT_TRANS](
	[TRANSSIN_REPC_ID] [int] NOT NULL,
	[TRANSSIN_OTRODETALLE] [varchar](255) NULL,
	[TRANSSIN_SIN_ID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REPORTE]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REPORTE](
	[Rep_Id] [int] IDENTITY(1,1) NOT NULL,
	[Rep_FechaRealizado] [datetime] NOT NULL,
	[Rep_ContactoSospechozo] [tinyint] NOT NULL,
	[Rep_ContactoConfirmado] [tinyint] NOT NULL,
	[Rep_Positivo] [tinyint] NOT NULL,
	[Rep_User_Id] [smallint] NOT NULL,
	[Rep_CantidadDosis] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Rep_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REPORTECOVID]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REPORTECOVID](
	[REPC_Id] [int] IDENTITY(1,1) NOT NULL,
	[REPC_Fecha] [datetime] NOT NULL,
	[REPC_User_Id] [smallint] NOT NULL,
	[REPC_Temperatura] [decimal](5, 2) NOT NULL,
	[REPC_Comentario] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[REPC_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SIGNOS]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SIGNOS](
	[Sig_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Sig_Detalle] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Sig_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SINTOMAS]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SINTOMAS](
	[Sint_Id] [int] IDENTITY(1,1) NOT NULL,
	[Sint_Detalle] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Sint_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioIDE]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioIDE](
	[User_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[User_Dni] [varchar](10) NOT NULL,
	[User_Nombres] [varchar](150) NOT NULL,
	[User_Apellidos] [varchar](150) NOT NULL,
	[User_Telefono] [varchar](11) NOT NULL,
	[User_Contrasenia] [varchar](150) NULL,
	[User_Per_Id] [tinyint] NULL,
	[User_HasCovid] [tinyint] NOT NULL,
	[User_Correo] [varchar](200) NULL,
	[User_Direccion] [varchar](200) NULL,
	[User_clave_temp] [varchar](150) NULL,
	[User_Estado] [smallint] NOT NULL,
	[AppId] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[version]    Script Date: 17/05/2022 05:35:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[version](
	[version] [varchar](5) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Index [Uk_Dni]    Script Date: 17/05/2022 05:35:38 p. m. ******/
ALTER TABLE [dbo].[UsuarioIDE] ADD  CONSTRAINT [Uk_Dni] UNIQUE NONCLUSTERED 
(
	[User_Dni] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CONCEPTOS] ADD  DEFAULT ((0)) FOR [con_estado]
GO
ALTER TABLE [dbo].[REPORTECOVID] ADD  DEFAULT ((36.00)) FOR [REPC_Temperatura]
GO
ALTER TABLE [dbo].[UsuarioIDE] ADD  DEFAULT ((0)) FOR [User_HasCovid]
GO
ALTER TABLE [dbo].[UsuarioIDE] ADD  DEFAULT ('') FOR [User_Correo]
GO
ALTER TABLE [dbo].[UsuarioIDE] ADD  DEFAULT ('') FOR [User_Direccion]
GO
ALTER TABLE [dbo].[UsuarioIDE] ADD  DEFAULT ((0)) FOR [User_Estado]
GO
ALTER TABLE [dbo].[REP_SINT_TRANSC]  WITH CHECK ADD FOREIGN KEY([TRANSC_Sint_id])
REFERENCES [dbo].[SINTOMAS] ([Sint_Id])
GO
ALTER TABLE [dbo].[REP_SINT_TRANSC]  WITH CHECK ADD FOREIGN KEY([TRANSC_Rep_id])
REFERENCES [dbo].[REPORTE] ([Rep_Id])
GO
ALTER TABLE [dbo].[REPC_SIG_TRANS]  WITH CHECK ADD FOREIGN KEY([TRANSC_REPC_ID])
REFERENCES [dbo].[REPORTECOVID] ([REPC_Id])
GO
ALTER TABLE [dbo].[REPC_SIG_TRANS]  WITH CHECK ADD FOREIGN KEY([TRANSC_SIG_ID])
REFERENCES [dbo].[SIGNOS] ([Sig_Id])
GO
ALTER TABLE [dbo].[REPC_SINT_TRANS]  WITH CHECK ADD FOREIGN KEY([TRANSSIN_REPC_ID])
REFERENCES [dbo].[REPORTECOVID] ([REPC_Id])
GO
ALTER TABLE [dbo].[REPC_SINT_TRANS]  WITH CHECK ADD FOREIGN KEY([TRANSSIN_SIN_ID])
REFERENCES [dbo].[SINTOMAS] ([Sint_Id])
GO
ALTER TABLE [dbo].[REPORTE]  WITH CHECK ADD FOREIGN KEY([Rep_User_Id])
REFERENCES [dbo].[UsuarioIDE] ([User_Id])
GO
ALTER TABLE [dbo].[REPORTECOVID]  WITH CHECK ADD FOREIGN KEY([REPC_User_Id])
REFERENCES [dbo].[UsuarioIDE] ([User_Id])
GO
ALTER TABLE [dbo].[UsuarioIDE]  WITH CHECK ADD  CONSTRAINT [FK_PersonPerf] FOREIGN KEY([User_Per_Id])
REFERENCES [dbo].[Perfil] ([Per_Id])
GO
ALTER TABLE [dbo].[UsuarioIDE] CHECK CONSTRAINT [FK_PersonPerf]
GO
